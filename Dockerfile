FROM python:3.11.3

WORKDIR /app

RUN pip install "poetry==$version 1.5.1"
RUN python -m venv /venv

COPY pyproject.toml poetry.lock ./
RUN poetry config virtualenvs.create false
RUN poetry install

COPY . .

EXPOSE 8000

CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0"]