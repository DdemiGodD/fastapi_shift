from pydantic import BaseModel


class UserForm(BaseModel):
    login: str
    password: str