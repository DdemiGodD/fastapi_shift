import hashlib
from datetime import datetime
from fastapi import Depends, HTTPException
from app.database import connect_db, AuthToken
from starlette import status

# token lifetime in seconds
MAX_LIFE_TIME = 60

SECRET = ''


def get_password_hash(password: str):
    return hashlib.sha256(f'{SECRET}{password}'.encode('utf8')).hexdigest()


def check_token(token: str, database=Depends(connect_db)):
    token = database.query(AuthToken).filter(AuthToken.token == token).one_or_none()
    if not token:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Invalid token')

    current_life_time = (datetime.utcnow() - token.creation_time).total_seconds()
    if current_life_time > MAX_LIFE_TIME:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Invalid token')

    return token

