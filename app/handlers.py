import uuid
from fastapi import APIRouter, Body, Depends, HTTPException
from fastapi.responses import RedirectResponse
from starlette import status
from app.forms import UserForm
from app.database import connect_db, User, AuthToken
from app.utils import get_password_hash, check_token

router = APIRouter()


@router.get("/")
def main():
    return RedirectResponse("http://127.0.0.1:8000/docs")


@router.post("/login", name='user:login')
def login(user_form: UserForm = Body(..., embed=True), database=Depends(connect_db)):
    user = database.query(User).filter(User.login == user_form.login).one_or_none()
    if not user or get_password_hash(user_form.password) != user.password:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='login/password incorrect')

    token = AuthToken(token=str(uuid.uuid4()), user_id=user.id)
    database.add(token)
    database.commit()

    return {'token': token.token}


@router.post('/user', name='user:create')
def create_user(user: UserForm = Body(..., embed=True), database=Depends(connect_db)):
    exist_user = database.query(User.id).filter(User.login == user.login).one_or_none()
    if exist_user:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail='Login in already used')

    new_user = User(
        login=user.login,
        password=get_password_hash(user.password)
    )
    database.add(new_user)
    database.commit()

    return {
        'user_id': new_user.id,
        'login': new_user.login,
    }


@router.get('/user', name='salary:get')
def get_salary(token: AuthToken = Depends(check_token), database=Depends(connect_db)):
    statistic = database.query(User).filter(User.id == token.user_id).one_or_none()
    if statistic:
        return {
            'salary': statistic.salary,
            'next_promotion': statistic.date_promotion,
        }
