from fastapi import FastAPI
from app.handlers import router
from app.database import create_db


def get_app() -> FastAPI:
    app = FastAPI()
    app.include_router(router)
    return app


create_db()
app = get_app()

