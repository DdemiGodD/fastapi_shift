import datetime
from random import randint, randrange
from sqlalchemy import create_engine
from sqlalchemy.orm import Session
from sqlalchemy import Column, Integer, String, ForeignKey, Float, DateTime, Date
from sqlalchemy.ext.declarative import declarative_base

DATE = datetime.datetime.now().date()
Base = declarative_base()

DATABASE_NAME = 'user_data.sqlite'
DATABASE_URL = f'sqlite:///{DATABASE_NAME}'


def connect_db():
    engine = create_engine(DATABASE_URL, connect_args={"check_same_thread": False})
    session = Session(bind=engine.connect())
    return session


def create_db():
    engine = create_engine(DATABASE_URL)
    Base.metadata.create_all(engine)


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    login = Column(String, nullable=False)
    password = Column(String, nullable=False)
    salary = Column(Float, default=randint(50000, 250000))
    date_promotion = Column(Date, default=DATE + datetime.timedelta(days=randrange(180, 361, 180)))


class AuthToken(Base):
    __tablename__ = 'auth_token'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('user.id'))
    token = Column(String)
    creation_time = Column(DateTime, default=datetime.datetime.utcnow)


if __name__ == '__main__':
    create_db()